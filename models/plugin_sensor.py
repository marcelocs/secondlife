# -*- coding: utf-8 -*-
import datetime

auth.settings.actions_disabled.append('register')
auth.settings.actions_disabled.append('change_password')
auth.settings.actions_disabled.append('request_reset_password')
auth.settings.actions_disabled.append('retrieve_username')
db.auth_user.first_name.readable = db.auth_user.first_name.writable = False
db.auth_user.last_name.readable = db.auth_user.last_name.writable = False
db.auth_user.email.writable = db.auth_user.email.readable = False
auth.settings.login_next = URL("plugin_sensor", "reports_visitors_per_hour")
auth.settings.logout_next = URL("plugin_sensor", "reports_visitors_per_hour")

db.define_table("plugin_sensor_visitor",
    Field("auth_user", "reference auth_user"),
    Field("owner_", "string"),
    Field("visitor", "string"),
    Field("coordinate", "string"),
    Field("place", "string"),
    Field("created", "datetime", default = datetime.datetime.utcnow())
)
