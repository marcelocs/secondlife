# -*- coding: utf-8 -*-
import json, datetime
def register_owner():
    db.auth_user.update_or_insert(db.auth_user.username == request.vars["owner"].replace(" ","___"), username = request.vars["owner"].replace(" ","___"), password = db.auth_user.password.validate(request.vars["password"])[0])
    return HTTP(200)

def register_visitor():
    auth_user = db(db.auth_user.username == request.vars["owner"].replace(" ","___"))
    if auth_user.isempty(): return HTTP(403)
    db.plugin_sensor_visitor.insert(auth_user = auth_user.select()[0].id, owner_ = request.vars["owner"].replace(" ","___"), visitor = request.vars["visitor"], coordinate = request.vars["coordinate"], place = request.vars["place"])
    return HTTP(200)

def user():
    print request.post_vars
    if "username" in request.post_vars:
        request.post_vars.username = request.vars.username = request.post_vars.username.replace(" ","___")
    return dict(form=auth())

@auth.requires_login()
def reports_visitors_per_hour():
    return dict()

@auth.requires_login()
def reports_visits_per_avatar():
    return dict()

@auth.requires_login()
def reports_visitors_all():
    return dict()

@auth.requires_login()
def visitors_per_hour():
    date_min = datetime.datetime.now() - datetime.timedelta(days = 7)
    visitors = {}
    date_list_visitors= {}
    date_list_ordened = []
    date_list_chart = []
    for visitor in db((db.plugin_sensor_visitor.auth_user == auth.user_id) & (db.plugin_sensor_visitor.created >= date_min)).select(orderby = ~db.plugin_sensor_visitor.created):
        date = "{:%Y-%m-%d %H}".format(visitor.created)
        if not date in date_list_ordened: date_list_ordened.append(date)
        if not date in visitors: visitors[date] = 0
        if not date in date_list_visitors: date_list_visitors[date] = []
        if not visitor.visitor in date_list_visitors[date]:
             date_list_visitors[date].append(visitor.visitor)
             visitors[date] += 1
    for date in visitors.keys(): date_list_chart.append([date + ":00:00", visitors[date]])
    return json.dumps({"visitors": visitors, "date_list_ordened": date_list_ordened, "date_list_chart": date_list_chart, "date_list_visitors": date_list_visitors })

@auth.requires_login()
def visits_per_avatar():
    date_min = datetime.datetime.now() - datetime.timedelta(days = 7)
    visitors = {}
    date_list_visitors= {}
    for visitor in db((db.plugin_sensor_visitor.auth_user == auth.user_id) & (db.plugin_sensor_visitor.created >= date_min)).select():
        date = "{:%Y-%m-%d}".format(visitor.created)
        if not visitor.visitor in visitors: visitors[visitor.visitor] = 0
        if not date in date_list_visitors: date_list_visitors[date] = []
        if not visitor.visitor in date_list_visitors[date]:
            date_list_visitors[date].append(visitor.visitor)
            visitors[visitor.visitor] += 1
    return json.dumps(visitors)

@auth.requires_login()
def visitors_all():
    date_min = datetime.datetime.now() - datetime.timedelta(hours = 1)
    visitors = []
    for visitor in db(
    (db.plugin_sensor_visitor.auth_user == auth.user_id)
    &
    (db.plugin_sensor_visitor.created >= date_min)
    ).select(orderby=~db.plugin_sensor_visitor.created):
        obj = {}
        obj["visitor"] = visitor["visitor"]
        obj["coordinate"] = visitor["coordinate"]
        obj["place"] = visitor["place"]
        obj["year"] = visitor["created"].year
        obj["month"] = visitor["created"].month
        obj["day"] = visitor["created"].day
        obj["hour"] = visitor["created"].hour
        obj["minute"] = visitor["created"].minute
        obj["second"] = visitor["created"].second
        visitors.append(obj)
    return json.dumps(visitors)
